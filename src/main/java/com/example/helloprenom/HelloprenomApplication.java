package com.example.helloprenom;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class HelloprenomApplication implements CommandLineRunner {

    public static void main(String[] args) {

        SpringApplication.run(HelloprenomApplication.class, args);

    }

    @Override
    public void run(String... args) throws Exception{
        System.out.println("Hello Adeline");
    }

}
